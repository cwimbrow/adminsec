from flask_admin.form import BaseForm
from flask_admin.model import BaseModelView
from flask_security import UserMixin, RoleMixin, current_user
from flask_security.datastore import Datastore, UserDatastore
from flask_security.utils import hash_password
from remodel.models import Model
from rethinkdb import r
from wtforms.fields import SelectField, SelectMultipleField
from wtforms.fields import StringField, TextAreaField
from wtforms.fields import PasswordField
from wtforms.fields import BooleanField


# need to run remodel.helpers.create_tables and
# remodel.helpers.create_indexes when new models are added
class User(Model, UserMixin):
    """ Remodel model mapped to users table in rethinkDB """

    has_and_belongs_to_many = ('Role', )

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    # properties allow flask-security access to remodel object fields
    @property
    def email(self):
        return self.fields.email

    @email.setter
    def email(self, value):
        self.update(email=value)

    @property
    def name(self):
        return self.fields.name

    @name.setter
    def name(self, value):
        self.update(name=value)

    @property
    def password(self):
        return self.fields.password

    @password.setter
    def password(self, value):
        self.update(password=value)

    @property
    def active(self):
        return self.fields.active

    @active.setter
    def active(self, value):
        self.update(active=value)

    @property
    def id(self):
        return self.fields.id

    @property
    def roles(self):
        return [role for role in self.fields.roles.all()]

    @property
    def entities(self):
        return self.fields.entities

    @property
    def one_role(self):
        try:
            return self.roles[0]
        except IndexError:
            return None

    def add_role(self, role):
        self['roles'].add(role)

    def remove_role(self, role):
        self['roles'].remove(role)

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class Role(Model, RoleMixin):
    """ Remodel model mapped to roles table in rethinkDB """
    has_and_belongs_to_many = ('User', )

    def __init__(self, *args, **kwargs):
        super(Role, self).__init__(*args, **kwargs)

    # properties allow flask-security access to remodel object fields
    @property
    def id(self):
        return self.fields.id

    @property
    def name(self):
        return self.fields.name

    @property
    def location(self):
        return self.fields.location

    @property
    def active(self):
        return self.fields.active

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class DemoEntity(Model):
    # properties allow flask-security access to remodel object fields
    @property
    def id(self):
        return self.fields.id

    @property
    def name(self):
        return self.fields.name

    @property
    def location(self):
        return self.fields.location

    @property
    def entity_id(self):
        return self.fields.entity_id

    @property
    def active(self):
        return self.fields.active

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class RoleForm(BaseForm):
    name = StringField('Name')
    description = StringField('Description')


class UserForm(BaseForm):
    name = StringField('Name')
    email = StringField('Email')
    password = PasswordField('Password')
    active = BooleanField('Active')
    entities = TextAreaField('Entities')
    roles = SelectMultipleField('Roles')

    def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, **kwargs):
        super(UserForm, self).__init__(formdata=formdata, obj=obj, prefix=prefix, data=data, meta=meta, **kwargs)
        # https://stackoverflow.com/questions/5519729/wtforms-how-to-select-options-in-selectmultiplefield
        self.roles.choices = [(role.id, role.name) for role in Role.all()]

        # only set the defaults on edit, obj is None when creating user
        if obj:
            self.name.default = obj.name
            self.email.default = obj.email
            self.active.default = obj.active
            self.entities.default = '\r\n'.join(obj.entities)
            self.roles.default = [role.id for role in obj.roles]
            self.process(formdata)


class DemoEntityForm(BaseForm):
    name = StringField('Name')
    location = StringField('Location')
    entity_id = SelectField('Entity ID')
    active = BooleanField('Active')

    def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, **kwargs):
        super(DemoEntityForm, self).__init__(formdata=formdata, obj=obj, prefix=prefix, data=data, meta=meta, **kwargs)

        self.entity_id.choices = [(ent, ent) for ent in current_user.fields.entities]

        if obj:
            self.name.default = obj.name
            self.location.default = obj.location
            self.entity_id.default = obj.entity_id
            self.active.default = obj.active
            self.process(formdata)


class DemoEntityView(BaseModelView):
    can_view_details = True
    column_list = ('name', 'location', 'entity_id', 'active')

    def __init__(self, args, **kwargs):
        super(DemoEntityView, self).__init__(args, **kwargs)

    def get_pk_value(self, model):
        return model.id

    def scaffold_list_columns(self):
        return ['name', 'location', 'entity_id', 'active']

    def scaffold_sortable_columns(self):
        return None

    def init_search(self):
        return False

    def get_list(self, page, sort_field, sort_desc, search, filters,
                 page_size=None):
        # filter entity list on current user's assigned entities
        entities = DemoEntity.objects.filter(
             lambda entity: r.expr(current_user.fields.entities).contains(
                 entity['entity_id']
             )
        )
        return len(entities), entities

    def get_one(self, id):
        return self.model.get(id=id)

    def create_model(self, form):
        return self.model.create(**form.data)

    def update_model(self, form, model):
        model.update(**form.data)
        return True

    def delete_model(self, model):
        model.delete()

    def is_valid_filter(self, filter):
        return True

    def scaffold_filters(self, name):
        return None

    def scaffold_form(self):
        return DemoEntityForm

    def scaffold_list_form(self, widget=None, validators=None):
        return DemoEntityForm

    def _create_ajax_loader(self, name, options):
        pass

    # set CRUD flags based on role of editor or reader
    def is_accessible(self):
        if current_user.is_authenticated and current_user.is_active:
            if current_user.has_role('editor'):
                self.can_create = True
                self.can_edit = True
                self.can_delete = True
                return True
            elif current_user.has_role('reader'):
                self.can_create = False
                self.can_edit = False
                self.can_delete = False
                return True
        return False


class UserModelView(BaseModelView):
    can_view_details = True
    column_list = ('name', 'email', 'active', 'roles')

    def get_pk_value(self, model):
        return model.id

    def scaffold_list_columns(self):
        return ['id', 'name', 'email', 'roles']

    def scaffold_sortable_columns(self):
        return None

    def init_search(self):
        return False

    def get_list(self, page, sort_field, sort_desc, search, filters,
                 page_size=None):
        return User.objects.count(), User.all()

    def get_one(self, id):
        return self.model.get(id=id)

    def create_model(self, form):
        # need to assign data to a variable since form.data generates
        # the dict when called, pop doesn't work otherwise
        data = form.data
        # role relations need to be added manually after model creation
        roles = data.pop('roles')

        # store entities as array of ids
        if data['entities']:
            data['entities'] = data['entities'].split('\r\n')

        data['password'] = hash_password(data['password'])
        model = self.model.create(**data)
        for role_id in roles:
            model.add_role(Role.get(role_id))
        return model

    def update_model(self, form, model):
        data = form.data
        roles = data.pop('roles')
        # only update password if it was changed in the form
        if data['password'] != '':
            data['password'] = hash_password(data['password'])
        else:
            data.pop('password')

        # store entities as array of ids
        if data['entities']:
            data['entities'] = data['entities'].split('\r\n')

        # clear current role relations and create new ones from form
        model['roles'].clear()
        for role_id in roles:
            model.add_role(Role.get(role_id))
        model.update(**data)
        return True

    def delete_model(self, model):
        model.delete()

    def is_valid_filter(self, filter):
        return True

    def scaffold_filters(self, name):
        return None

    def scaffold_form(self):
        return UserForm

    def scaffold_list_form(self, widget=None, validators=None):
        return UserForm

    def _create_ajax_loader(self, name, options):
        pass

    # only basic authentication check for now
    def is_accessible(self):
        return current_user.is_authenticated and current_user.is_active and \
               current_user.has_role('admin')


class RoleModelView(BaseModelView):
    can_view_details = True

    def get_pk_value(self, model):
        return model.id

    def scaffold_list_columns(self):
        return ['id', 'name', 'description']

    def scaffold_sortable_columns(self):
        return None

    def init_search(self):
        return False

    def scaffold_form(self):
        return RoleForm

    def get_list(self, page, sort_field, sort_desc, search, filters,
                 page_size=None):
        return Role.objects.count(), Role.all()

    def get_one(self, id):
        return self.model.get(id=id)

    def create_model(self, form):
        return self.model.create(**form.data)

    def update_model(self, form, model):
        model.update(**form.data)
        return True

    def delete_model(self, model):
        model.delete()

    def is_valid_filter(self, filter):
        return True

    def scaffold_filters(self, name):
        return None

    def scaffold_list_form(self, widget=None, validators=None):
        return None

    def _create_ajax_loader(self, name, options):
        pass

    # only basic authentication check for now
    def is_accessible(self):
        return current_user.is_authenticated and current_user.is_active and \
               current_user.has_role('admin')


class RethinkDBDatastore(Datastore):
    def put(self, model):
        model.save()
        return model

    def delete(self, model):
        model.delete()


class RethinkDBUserDatastore(RethinkDBDatastore, UserDatastore):
    """ A RethinkDB datastore implementation for Flask-Security that assumes
        the use of the flask-rethinkdb extension.
    """
    def __init__(self, db, user_model, role_model):
        RethinkDBDatastore.__init__(self, db)
        UserDatastore.__init__(self, user_model, role_model)

    def get_user(self, identifier):
        results = User.get(id=identifier)
        if not results:
            return User.get(email=identifier)

    def find_user(self, **query):
        results = User.filter(**query)
        if len(results) == 1:
            return results[0]
        return results

    def find_role(self, **query):
        results = Role.filter(**query)
        if len(results) == 1:
            return results[0]
        return results

    def add_role_to_user(self, user, role):
        user['roles'].add(role)

    def remove_role_from_user(self, user, role):
        user['roles'].remove(role)

    def toggle_active(self, user):
        user.update(active=not user.fields.active)
        return True

    def deactivate_user(self, user):
        if user.fields.active:
            user.update(active=False)
            return True
        return False

    def activate_user(self, user):
        if not user.fields.active:
            user.update(active=True)
            return True
        return False

    def create_role(self, **role):
        return Role.create(**role)

    def create_user(self, **user):
        return User.create(**user)

    def delete_user(self, user):
        user.delete()
