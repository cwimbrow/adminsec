
DEBUG = True
SECRET_KEY = 'nkt-admin secret key'

# Flask Admin setup options
FLASK_ADMIN_SWATCH = 'cerulean'

# Flask-Security config
SECURITY_EMAIL_SENDER = 'nicholas.tulach+nkt-admin@temboinc.com'

# salt must be consistent or login will fail
SECURITY_PASSWORD_SALT = b'$2b$12$h4b59HedYX3MP4KvsWsJEu'
SECURITY_LOGIN_WITHIN = '2 hours'

SECURITY_POST_LOGIN_VIEW = '/admin'

# Flask-Mail config
MAIL_SERVER = 'smtp.sparkpostmail.com'
# GCP blocks 587, 25
# APP.config['MAIL_PORT'] = 587
MAIL_PORT = 2525
MAIL_USE_TLS = True
MAIL_USERNAME = 'SMTP_Injection'
MAIL_PASSWORD = '3799baa229dcffe11ae4a1f98b11d70c9627776d'
