import os

from flask import Flask, url_for, redirect
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_rethinkdb import RethinkDB
from flask_security import Security
from remodel.connection import pool

# pool needs to be configured before the modelview imports as remodel's
# lazy evaluation does not seem to work (see: UserForm.roles.choices)
pool.configure(host='35.222.120.250',
               port=28015,
               user=os.environ.get('DB_USER'),
               password=os.environ.get('DB_PWD'),
               db='cw_tt')

from models import User, Role, DemoEntity
from models import UserModelView, RoleModelView, DemoEntityView
from models import RethinkDBUserDatastore

APP = Flask(__name__, template_folder='templates')
APP.config.from_pyfile('config.py')

ADMIN = Admin(APP, name='admindemo', template_mode='bootstrap3')
ADMIN.add_view(UserModelView(User))
ADMIN.add_view(RoleModelView(Role))
ADMIN.add_view(DemoEntityView(DemoEntity))

DB = RethinkDB(APP)
USER_DATASTORE = RethinkDBUserDatastore(DB, User, Role)
SECURITY = Security(APP, USER_DATASTORE)

# context injector for integrating flask-admin templates with security
# https://flask-admin.readthedocs.io/en/latest/introduction/#using-flask-security
@SECURITY.context_processor
def security_context_processor():
    return dict(
        admin_base_template=ADMIN.base_template,
        admin_view=ADMIN.index_view,
        h=admin_helpers,
        get_url=url_for,
    )


@APP.route('/')
def index():
    return redirect(url_for('security.login'))


if __name__ == '__main__':
    APP.run()
